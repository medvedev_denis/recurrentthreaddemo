package ru.md.interferens;

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static int i;

    InterferenceThread(String name, InterferenceExample checker){
        super(name);
        this.checker = checker;
    }

    public void run() {
        System.out.println(this.getName() + " Запущен");
        while (!checker.stop()){
            increment();
        }
        System.out.println(this.getName() + " Завершен");
    }

    private void increment() {
        ++i;
    }

    public int getI() {
        return i;
    }
}
