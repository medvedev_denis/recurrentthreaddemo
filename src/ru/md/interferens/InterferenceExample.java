package ru.md.interferens;

import java.util.concurrent.atomic.AtomicInteger;

public class InterferenceExample {
    private static final int HUNDRED_MILLION = 100_000_000;
    private AtomicInteger counter = new AtomicInteger();

    public boolean stop() {
        return counter.incrementAndGet() > HUNDRED_MILLION;
    }

    public void example() throws InterruptedException {
        InterferenceThread firstThread = new InterferenceThread("Поток 1",this);
        InterferenceThread secondThread = new InterferenceThread("Поток 2",this);
        firstThread.start();
        secondThread.start();
        firstThread.join();
        secondThread.join();
        System.out.printf("Ожидание %d\nПолучили %d", HUNDRED_MILLION, firstThread.getI());
    }
}
