package ru.md.runnable;

public class HelloRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("Hello from a thread!");
    }

    public static void main(String[] args) throws InterruptedException {
        (new Thread(new HelloRunnable())).start();
        Thread.sleep(5000);
        System.out.println("Hello from main thread!");
    }
}
