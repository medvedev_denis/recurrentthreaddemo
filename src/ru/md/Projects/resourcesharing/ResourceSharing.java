package ru.md.Projects.resourcesharing;

public class ResourceSharing extends Thread {

    private static volatile Integer i = 0;

    ResourceSharing(String threadName){
        this.setName(threadName);
    }

    public void run() {
            int counter = 0;
            for (; counter < 50; counter++) {
                increment();
                System.out.println(this.getName() + "; i: " + i);
            }
    }

    private void increment() {
        synchronized (i) {
            ++i;
        }
    }

    public static int getI() {
        return i;
    }

}
