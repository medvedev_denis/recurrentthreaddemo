package ru.md.Projects.ReadingData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ReadingData {
    public static void main(String[] args) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("src\\ru\\md\\Projects\\ReadingData\\resultfile.txt"));
        FileWork firstThread = new FileWork("src\\ru\\md\\Projects\\ReadingData\\firstfile.txt", writer);
        FileWork secondThread = new FileWork("src\\ru\\md\\Projects\\ReadingData\\secondfile.txt", writer);
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (firstThread.isInterrupted() & secondThread.isInterrupted()){
            writer.close();
        }
    }
}
