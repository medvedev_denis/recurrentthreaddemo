package ru.md.Projects.ChickenEgg;

public class ChickenEgg extends Thread{

    public void run(){
        for (int i = 0 ; i < 5 ; i++) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {e.printStackTrace();}
            System.out.println("Яйцо!");
        }
    }

    private static ChickenEgg egg = new ChickenEgg();

    public static void main(String[] args) {
        System.out.println("Спор начат...");
        egg.start();
        for(int i = 0; i < 5; i++){
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){e.printStackTrace();}

            System.out.println("Курица!");
        }
        if(egg.isAlive()) {
            try{
                egg.join();
            } catch(InterruptedException e){e.printStackTrace();}
            System.out.println("Первым появилось яйцо!");
        } else {
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}
