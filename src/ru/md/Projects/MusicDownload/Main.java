package ru.md.Projects.MusicDownload;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    private static final String IN_FILE_TXT = "src\\ru\\md\\Projects\\MusicDownload\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\md\\Projects\\MusicDownload\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\md\\Projects\\MusicDownload\\music\\music";

    public static void main(String[] args) {
        savingDownloadLinks();
        String music;
        int count = 1;
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            while ((music = musicFile.readLine()) != null) {
                downloadUsingNIO(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                System.out.printf("Файл %d загружен!\n", count);
                count++;
            }
            System.out.println("Все фийлы загружены!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static void savingDownloadLinks(){
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);

                String result;
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    result = bufferedReader.lines().collect(Collectors.joining("\n"));
                }
                Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
                Matcher matcher = email_pattern.matcher(result);
                int i = 0;
                while (matcher.find() && i < 2) {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
            }
            System.out.println("Все ссылки сохранены!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
